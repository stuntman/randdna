#include "rdna.hpp"




int main()
{
	string bases;
	int seed, out;
	
	cout << "Enter bases";
	cin >> bases;
	
	cout << "Please choose a random number seed between 1-9 \n";
	cin >> seed;

	cout << "Please input the length of the random sequence to generated \n";
	cin >> out;

	cout << randDNA(seed, bases, out);
	
	return 0;
		
}
