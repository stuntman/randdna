// Computing 2 Lab 2.cpp Random DNA base generator i.e bases = ATCG 
//
#include <iostream>
#include <string>
#include <random>
#include <vector>

using namespace std;

string randDNA(int s, string b, int n)
{
	string characters = b;
	int output = n;
	int seed = s;
	int min = 0;
	int max = -1;
	string dna;
	char c = 0;

	for ( unsigned int i = 0; i < characters.size(); i++){
		max = max + 1;
	}
	
	std::mt19937 eng(seed);
	std::uniform_int_distribution<>uniform(min, max);


	for (int x = 0; x < output; x++) {
		c = characters[uniform(eng)];
		dna += c;
	};

	return dna;

}




